'use strict';

var prompt = require('prompt');
var JiraApi = require('jira').JiraApi;
var _ = require('underscore');

var config = { "host":"jira.icehealthsystems.com",
           "port":443,
           "username":"",
           "password":"",
           "asset":""};

var AssetQueries = [{"asset":"VPC",
                     "query": "issuetype = \"ST: Cloud VPC\" and status != Closed and (labels not in (\"reviewedbyitmanager\") or labels is EMPTY )"},
                     {"asset":"CA",
                     "query": "issuetype = \"ST:Cloud Asset\" and status != Closed and (labels in (\"app-stack\"))"},
                     {"asset":"DA",
                     "query": "issuetype = \"ST: Data Asset\" and status != Closed"},
                     {"asset":"S3",
                     "query": "issuetype = \"ST:Cloud Asset\" and labels in (\"s3-bucket\") and status != Closed"}
                   ];

if ( process.argv.length < 2 ) {
	console.log("node index <KEY>");
	process.exit(-1);
}

var promptloginschema = { "properties": {
    "username": {
        "description": 'Enter Jira username',
        "required": true
    },
    "password": {
      "hidden": true
    },
    "asset": {
        "description": 'VPC, CA (Coud Asset), DA (Data Asset), S3 (S3 Bucket)',
        "required": true
    }
  }
};

var promptkeyschema = { "properties": {
    "key": {
      "description": 'Enter Jira key',
      "required": true
    }
  }
};

var promptcontinueschema = { "properties": {
    "continue": {
      "description": 'Do you want to search for another key (y/n)',
      "required": true
    }
  }
};

var login = function (callback) {
  prompt.start();
  prompt.get(promptloginschema, function (err, result) {
    config.username = result.username;
    config.password = result.password;
    config.asset = result.asset;
    callback();
  })
}

var printoutrpt = function (issue) {
  console.log("summary: " + issue.fields.summary +" | key: " + issue.key + " has " + issue.fields.issuelinks.length + " issue links" +  " | status: " + issue.fields.status.name);
}

var getIssue = function(jiraconn, id, callback) {
  jiraconn.getIssue (id, function (err, issue) {
    if (err) {
      callback(err);
    } else {
      callback(null, issue);
    }
  });
}

var findJIRAIssue = function (asset, jiraconn, callback) {
  var records = [];
  var record = _.find( AssetQueries, function ( x ) {
    return x.asset === asset.toUpperCase();
  });

  if (!record) {
    callback ("Invalid asset type");
  } else {
    console.log("Searching for " + record.asset + " assets");
    jiraconn.searchJira(record.query,'', function (err, results) {
          if (err) {
            callback(err)
          } else {
            for (var i = 0; i < results.issues.length; i++) {
              getIssue(jiraconn, results.issues[i].id, function (err, record) {
                if (err) {
                  console.log(err);
                } else {
//                  console.log(record);
                  records.push(JSON.parse(JSON.stringify(record)));
                }
              });
          }
        }
    });
    console.log(records.length);
    callback(null, records);
  }
}

var main = function() {
  login ( function(err) {
    var jira = new JiraApi('https', config.host, config.port, config.username, config.password, '2');
    findJIRAIssue(config.asset, jira, function (err, records) {
        if (err) {
          console.log(err);
        } else {
          console.log(records.length);
        }
    });
  });
}

main()
